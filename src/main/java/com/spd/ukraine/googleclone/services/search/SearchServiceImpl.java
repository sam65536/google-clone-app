package com.spd.ukraine.googleclone.services.search;

import com.spd.ukraine.googleclone.domain.WebResource;
import com.spd.ukraine.googleclone.repositories.LuceneIndexer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.apache.lucene.queryparser.classic.QueryParser.Operator.AND;

@Service
public class SearchServiceImpl implements SearchService {

    private static final Logger logger = LogManager.getLogger(SearchServiceImpl.class);

    private volatile IndexReader indexReader;
    private final LuceneIndexer luceneIndexer;

    @Autowired
    public SearchServiceImpl(LuceneIndexer luceneIndexer) {
        this.luceneIndexer = luceneIndexer;
    }

    @Override
    public List<WebResource> search(String query, int from, int count) {
        List<WebResource> result = new ArrayList<>();
        try {
            QueryParser parser = new MultiFieldQueryParser(new String[]{"title", "content"}, luceneIndexer.getAnalyzer());
            parser.setDefaultOperator(AND);
            Query q = parser.parse(query);
            int nDocs = from + count;
            indexReader = DirectoryReader.open(luceneIndexer.getDirectory());
            IndexSearcher searcher = new IndexSearcher(indexReader);
            TopScoreDocCollector collector = TopScoreDocCollector.create(nDocs);
            searcher.search(q, collector);
            TopDocs topDocs = collector.topDocs(from, count);
            ScoreDoc[] scoreDocs = topDocs.scoreDocs;
            logger.debug("found {} hits.", scoreDocs.length);
            result = takeResults(searcher, scoreDocs, q);
        } catch (Exception e) {
            logger.error("invalid search query! ", e.getMessage());
        }
        return result;
    }

    private List<WebResource> takeResults(IndexSearcher searcher, ScoreDoc[] scoreDocs, Query query) {
        List<WebResource> result = new ArrayList<>();
        QueryScorer scorer = new QueryScorer(query);
        Analyzer analyzer = luceneIndexer.getAnalyzer();
        SimpleHTMLFormatter formatter = new SimpleHTMLFormatter("<span class=\"highlight\">", "</span>");
        Highlighter highlighter = new Highlighter(formatter, scorer);
        highlighter.setTextFragmenter(new SimpleSpanFragmenter(scorer, 330));
        try {
            for (ScoreDoc scoreDoc : scoreDocs) {
                Document document = searcher.doc(scoreDoc.doc);
                WebResource resource = new WebResource();
                resource.setUrl(document.get("url"));
                String title = document.get("title");
                resource.setTitle(highlighter.getBestFragment(analyzer, "title", title));
                String content = document.get("content");
                resource.setContent(highlighter.getBestFragment(analyzer, "content", content));
                result.add(resource);
            }
        } catch (Exception e) {
            logger.error("error while obtaining search results: ", e.getMessage());
        }
        return result;
    }
}
