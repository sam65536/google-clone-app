package com.spd.ukraine.googleclone.services.crawl;

import com.spd.ukraine.googleclone.repositories.LuceneIndexer;
import com.spd.ukraine.googleclone.crawler.WebResourceCrawlFactory;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrawlServiceImpl implements CrawlService {

    private static final Logger logger = LogManager.getLogger(CrawlService.class);
    private static final int NUMBER_OF_CRAWLERS = 10;

    private final LuceneIndexer luceneIndexer;

    @Autowired
    public CrawlServiceImpl(LuceneIndexer luceneIndexer) {
        this.luceneIndexer = luceneIndexer;
    }

    @Override
    public void index(String url, int depth) {
        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(
                this.getClass().getSimpleName().substring(0,5).toUpperCase());
        config.setPolitenessDelay(100);
        config.setResumableCrawling(true);
        config.setFollowRedirects(false);
        config.setUserAgentString("Mozilla");
        config.setMaxDepthOfCrawling(depth);
        try {
            PageFetcher pageFetcher = new PageFetcher(config);
            RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
            RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
            CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);
            controller.addSeed(url);
            controller.startNonBlocking(new WebResourceCrawlFactory(luceneIndexer), NUMBER_OF_CRAWLERS);
            logger.info("crawl successfully started!");
        } catch (Exception e) {
            logger.error("something wrong while crawl instantiation: ", e.getMessage());
        }
    }
}
