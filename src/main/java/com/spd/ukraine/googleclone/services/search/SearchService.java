package com.spd.ukraine.googleclone.services.search;

import com.spd.ukraine.googleclone.domain.WebResource;

import java.util.List;

public interface SearchService {
    List<WebResource> search(String query, int from, int count);
}
