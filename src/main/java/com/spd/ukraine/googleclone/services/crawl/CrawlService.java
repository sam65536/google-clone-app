package com.spd.ukraine.googleclone.services.crawl;

public interface CrawlService {
    void index(String url, int depth);
}
