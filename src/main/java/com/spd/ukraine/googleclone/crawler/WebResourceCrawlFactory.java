package com.spd.ukraine.googleclone.crawler;

import com.spd.ukraine.googleclone.repositories.LuceneIndexer;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import org.springframework.beans.factory.annotation.Autowired;

public class WebResourceCrawlFactory implements CrawlController.WebCrawlerFactory<WebResourceCrawler> {

    private final LuceneIndexer luceneIndexer;

    @Autowired
    public WebResourceCrawlFactory(LuceneIndexer luceneIndexer) {
        this.luceneIndexer = luceneIndexer;
    }

    @Override
    public WebResourceCrawler newInstance() {
        return new WebResourceCrawler(luceneIndexer);
    }
}
