package com.spd.ukraine.googleclone.crawler;

import com.spd.ukraine.googleclone.domain.WebResource;

import com.spd.ukraine.googleclone.repositories.LuceneIndexer;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;
import java.util.regex.Pattern;

public class WebResourceCrawler extends WebCrawler {

    private static final Logger logger = LogManager.getLogger(WebResourceCrawler.class);

    private static Pattern FILE_ENDING_EXCLUSION_PATTERN = Pattern.compile(".*(\\.(" +
                "css|js" +
                "|bmp|gif|jpe?g|JPE?G|png|tiff?|ico|nef|raw" +
                "|mid|mp2|mp3|mp4|wav|wma|flv|mpe?g" +
                "|avi|mov|mpeg|ram|m4v|wmv|rm|smil" +
                "|pdf|doc|docx|pub|xls|xlsx|vsd|ppt|pptx" +
                "|swf" +
                "|zip|rar|gz|bz2|7z|bin" +
                "|xml|txt|java|c|cpp|exe" +
                "|vk.com?|wordpress?|" +
                "))$");

    private final LuceneIndexer luceneIndexer;

    @Autowired
    public WebResourceCrawler(LuceneIndexer luceneIndexer) {
        this.luceneIndexer = luceneIndexer;
    }

    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        return !FILE_ENDING_EXCLUSION_PATTERN.matcher(href).matches();
    }

    @Override
    public void visit(Page page) {
        String url = page.getWebURL().getURL();
        if (page.getParseData() instanceof HtmlParseData) {
            HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
            String title = htmlParseData.getTitle();
            String content = htmlParseData.getText();
            Set<WebURL> links = htmlParseData.getOutgoingUrls();

            logger.info("URL: {}", url);
            logger.info("title: {} ", title);
            logger.info("number of outgoing links: {}", links.size());
            logger.info("content length: {}", content.length());

            WebResource resource = new WebResource(url, title, content);
            try {
                luceneIndexer.add(resource);
                logger.info("successfully stored!");

            } catch (Exception e) {
                logger.error("for {} ", url, e.getMessage());
            }
        }
    }
}
