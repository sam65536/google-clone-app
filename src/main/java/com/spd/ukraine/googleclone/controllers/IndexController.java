package com.spd.ukraine.googleclone.controllers;

import com.spd.ukraine.googleclone.services.crawl.CrawlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller("/index")
public class IndexController {

    private final CrawlService crawlService;

    @Autowired
    public IndexController(CrawlService crawlService) {
        this.crawlService = crawlService;
    }

    @GetMapping
    public String index() {
        return "index";
    }

    @PostMapping
    public String addUrl(@RequestParam("q") String url, @RequestParam("depth") int depth, Model model) {
        model.addAttribute("url", url);
        crawlService.index(url, depth);
        return "index";
    }
}
