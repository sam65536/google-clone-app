package com.spd.ukraine.googleclone.controllers;

import com.spd.ukraine.googleclone.services.search.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SearchController {

    private static final int MAX_AMOUNT_OF_RESULTS = 1000;
    private static final int DEFAULT_PAGE_SIZE = 10;

    private final SearchService searchService;

    @Autowired
    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping("/")
    public String root() {
        return "search";
    }

    @GetMapping("/search")
    public String search(@RequestParam("q") String query, Model model) {
        model.addAttribute("query", query);
        return "redirect:search/{query}/page/1";
    }

    @GetMapping("/search/{query}/page/{pageNumber}")
    public String search(@PathVariable String query, @PathVariable Integer pageNumber, Model model) {
        PagedListHolder pagedListHolder = new PagedListHolder<>((searchService.search(query, 0, MAX_AMOUNT_OF_RESULTS)));
        pagedListHolder.setPageSize(DEFAULT_PAGE_SIZE);
        final int goToPage = pageNumber - 1;
        if (goToPage <= pagedListHolder.getPageCount() && (goToPage >= 0)) {
            pagedListHolder.setPage(goToPage);
        }

        int current = pagedListHolder.getPage() + 1;
        int begin = Math.max(1, pageNumber - DEFAULT_PAGE_SIZE + 1);
        int end = Math.min(begin + DEFAULT_PAGE_SIZE - 1, pagedListHolder.getPageCount());
        int totalPageCount = pagedListHolder.getPageCount();

        String baseUrl = "/search/" + query + "/page/";

        model.addAttribute("beginIndex", begin);
        model.addAttribute("endIndex", end);
        model.addAttribute("currentIndex", current);
        model.addAttribute("totalPageCount", totalPageCount);
        model.addAttribute("baseUrl", baseUrl);
        model.addAttribute("results", pagedListHolder.getPageList());
        return "result";
    }
}
