package com.spd.ukraine.googleclone.repositories;

import com.spd.ukraine.googleclone.domain.WebResource;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Paths;

import static org.apache.lucene.document.Field.Store.YES;
import static org.apache.lucene.index.IndexWriterConfig.OpenMode.CREATE_OR_APPEND;

@Getter
@Setter
@Repository
public class LuceneIndexer {

	private static final Logger logger = LogManager.getLogger(LuceneIndexer.class);

	private IndexWriter indexWriter;
	private Analyzer analyzer;
	private Directory directory;

	@Autowired
	public LuceneIndexer() {
		analyzer = new StandardAnalyzer();
		IndexWriterConfig iwConfig = new IndexWriterConfig(analyzer);
		iwConfig.setOpenMode(CREATE_OR_APPEND);
		try {
			directory = FSDirectory.open(Paths.get(
			        this.getClass().getSimpleName().substring(0,6).toUpperCase()));
			indexWriter = new IndexWriter(directory, iwConfig);
		} catch (IOException e) {
			logger.error("failed to create index directory: ", e.getMessage());
		}
	}

	public void add(WebResource resource) {
		Document document = new Document();
		document.add(new StringField("url", resource.getUrl(), YES));
		document.add(new TextField("title", resource.getTitle(), YES));
		document.add(new TextField("content", resource.getContent(), YES));
		try {
			indexWriter.addDocument(document);
			indexWriter.commit();
		} catch (IOException e) {
			logger.error("failed to index a document: ", e.getMessage());
		}
	}
}
